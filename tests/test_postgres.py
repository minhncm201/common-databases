import pytest
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncEngine
from common_databases.postgres import create_postgres_engine, PostgresSettings


@pytest.fixture(scope="module")
async def postgres_engine():
    settings = PostgresSettings(_env_file=".env")
    engine = create_postgres_engine(
        settings.POSTGRES_HOST, 
        settings.POSTGRES_PORT, 
        settings.POSTGRES_DB_NAME, 
        settings.POSTGRES_USER, 
        settings.POSTGRES_PASSWORD
    )
    yield engine
    await engine.dispose()


async def test_connection(postgres_engine: AsyncEngine):
    async with postgres_engine.connect() as conn:
        await conn.execute(text("select 1"))