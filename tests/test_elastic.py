import pytest
from elasticsearch import AsyncElasticsearch

from common_databases.elastic import create_elastic_client, ElasticSettings


@pytest.fixture(scope="module")
async def elastic_client():
    settings = ElasticSettings(_env_file=".env")
    client = create_elastic_client(
        settings.ELASTIC_HOSTS,
        settings.ELASTIC_USER,
        settings.ELASTIC_PASSWORD,
        settings.ELASTIC_CLOUD_ID,
        settings.ELASTIC_CA_CERTS,
        settings.ELASTIC_API_KEY,
    )
    yield client
    await client.close()


async def test_connection(elastic_client: AsyncElasticsearch):
    response = await elastic_client.cat.indices(index="*")