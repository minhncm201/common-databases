from sqlalchemy.ext.asyncio import create_async_engine, AsyncEngine


def create_postgres_engine(
    host: str,
    port: str,
    db_name: str = None,
    user: str = None,
    password: str = None
) -> AsyncEngine:
    creds = ""
    if user and password:
        creds = f"{user}:{password}@"
    db = "" if db_name is None else f"/{db_name}"
    uri = f"postgresql+asyncpg://{creds}{host}:{port}{db}"
    engine = create_async_engine(url=uri)
    return engine