from typing import Optional

from common_databases.abtractions import BasePydanticSettings


class PostgresSettings(BasePydanticSettings):
    POSTGRES_HOST: Optional[str] = None
    POSTGRES_PORT: Optional[str] = None
    POSTGRES_DB_NAME: Optional[str] = None
    POSTGRES_USER: Optional[str] = None
    POSTGRES_PASSWORD: Optional[str] = None