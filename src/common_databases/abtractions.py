from threading import Lock
from typing import Tuple, Type
from typing_extensions import Self
from pydantic_settings import BaseSettings, PydanticBaseSettingsSource, SettingsConfigDict


class Singleton:
    _instances = {}
    _lock = Lock()

    def __new__(cls) -> Self:
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__new__(cls)
                cls._instances[cls] = instance
            return cls._instances[cls]
        

class BasePydanticSettings(BaseSettings):
    model_config = SettingsConfigDict(extra="ignore")

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        return init_settings, dotenv_settings, env_settings, file_secret_settings