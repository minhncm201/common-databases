import elasticsearch


def create_elastic_client(
    hosts: str = None,
    user: str = None,
    password: str = None,
    cloud_id: str = None,
    ca_certs: str = None,
    api_key: str = None
) -> elasticsearch.AsyncElasticsearch:
    nodes = hosts.split(",") if isinstance(hosts, str) else None
    if user is None and password is None:
        BASIC_AUTH = None
    else:
        BASIC_AUTH = (user, password)

    elastic_client = elasticsearch.AsyncElasticsearch(
        hosts=nodes,
        cloud_id=cloud_id,
        basic_auth=BASIC_AUTH,
        ca_certs=ca_certs,
        api_key=api_key,
    )
    return elastic_client